module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
// eslint-disable-next-line func-names
module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return "@media ".concat(item[2], " {").concat(content, "}");
      }

      return content;
    }).join('');
  }; // import a list of modules into the list
  // eslint-disable-next-line func-names


  list.i = function (modules, mediaQuery, dedupe) {
    if (typeof modules === 'string') {
      // eslint-disable-next-line no-param-reassign
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    if (dedupe) {
      for (var i = 0; i < this.length; i++) {
        // eslint-disable-next-line prefer-destructuring
        var id = this[i][0];

        if (id != null) {
          alreadyImportedModules[id] = true;
        }
      }
    }

    for (var _i = 0; _i < modules.length; _i++) {
      var item = [].concat(modules[_i]);

      if (dedupe && alreadyImportedModules[item[0]]) {
        // eslint-disable-next-line no-continue
        continue;
      }

      if (mediaQuery) {
        if (!item[2]) {
          item[2] = mediaQuery;
        } else {
          item[2] = "".concat(mediaQuery, " and ").concat(item[2]);
        }
      }

      list.push(item);
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || ''; // eslint-disable-next-line prefer-destructuring

  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return "/*# sourceURL=".concat(cssMapping.sourceRoot || '').concat(source, " */");
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = "sourceMappingURL=data:application/json;charset=utf-8;base64,".concat(base64);
  return "/*# ".concat(data, " */");
}

/***/ }),

/***/ "./node_modules/object-assign/index.js":
/*!*********************************************!*\
  !*** ./node_modules/object-assign/index.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/*
object-assign
(c) Sindre Sorhus
@license MIT
*/


/* eslint-disable no-unused-vars */
var getOwnPropertySymbols = Object.getOwnPropertySymbols;
var hasOwnProperty = Object.prototype.hasOwnProperty;
var propIsEnumerable = Object.prototype.propertyIsEnumerable;

function toObject(val) {
	if (val === null || val === undefined) {
		throw new TypeError('Object.assign cannot be called with null or undefined');
	}

	return Object(val);
}

function shouldUseNative() {
	try {
		if (!Object.assign) {
			return false;
		}

		// Detect buggy property enumeration order in older V8 versions.

		// https://bugs.chromium.org/p/v8/issues/detail?id=4118
		var test1 = new String('abc');  // eslint-disable-line no-new-wrappers
		test1[5] = 'de';
		if (Object.getOwnPropertyNames(test1)[0] === '5') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test2 = {};
		for (var i = 0; i < 10; i++) {
			test2['_' + String.fromCharCode(i)] = i;
		}
		var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
			return test2[n];
		});
		if (order2.join('') !== '0123456789') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test3 = {};
		'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
			test3[letter] = letter;
		});
		if (Object.keys(Object.assign({}, test3)).join('') !==
				'abcdefghijklmnopqrst') {
			return false;
		}

		return true;
	} catch (err) {
		// We don't expect any of the above to throw, but better to be safe.
		return false;
	}
}

module.exports = shouldUseNative() ? Object.assign : function (target, source) {
	var from;
	var to = toObject(target);
	var symbols;

	for (var s = 1; s < arguments.length; s++) {
		from = Object(arguments[s]);

		for (var key in from) {
			if (hasOwnProperty.call(from, key)) {
				to[key] = from[key];
			}
		}

		if (getOwnPropertySymbols) {
			symbols = getOwnPropertySymbols(from);
			for (var i = 0; i < symbols.length; i++) {
				if (propIsEnumerable.call(from, symbols[i])) {
					to[symbols[i]] = from[symbols[i]];
				}
			}
		}
	}

	return to;
};


/***/ }),

/***/ "./node_modules/prop-types/checkPropTypes.js":
/*!***************************************************!*\
  !*** ./node_modules/prop-types/checkPropTypes.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */



var printWarning = function() {};

if (true) {
  var ReactPropTypesSecret = __webpack_require__(/*! ./lib/ReactPropTypesSecret */ "./node_modules/prop-types/lib/ReactPropTypesSecret.js");
  var loggedTypeFailures = {};
  var has = Function.call.bind(Object.prototype.hasOwnProperty);

  printWarning = function(text) {
    var message = 'Warning: ' + text;
    if (typeof console !== 'undefined') {
      console.error(message);
    }
    try {
      // --- Welcome to debugging React ---
      // This error was thrown as a convenience so that you can use this stack
      // to find the callsite that caused this warning to fire.
      throw new Error(message);
    } catch (x) {}
  };
}

/**
 * Assert that the values match with the type specs.
 * Error messages are memorized and will only be shown once.
 *
 * @param {object} typeSpecs Map of name to a ReactPropType
 * @param {object} values Runtime values that need to be type-checked
 * @param {string} location e.g. "prop", "context", "child context"
 * @param {string} componentName Name of the component for error messages.
 * @param {?Function} getStack Returns the component stack.
 * @private
 */
function checkPropTypes(typeSpecs, values, location, componentName, getStack) {
  if (true) {
    for (var typeSpecName in typeSpecs) {
      if (has(typeSpecs, typeSpecName)) {
        var error;
        // Prop type validation may throw. In case they do, we don't want to
        // fail the render phase where it didn't fail before. So we log it.
        // After these have been cleaned up, we'll let them throw.
        try {
          // This is intentionally an invariant that gets caught. It's the same
          // behavior as without this statement except with a better message.
          if (typeof typeSpecs[typeSpecName] !== 'function') {
            var err = Error(
              (componentName || 'React class') + ': ' + location + ' type `' + typeSpecName + '` is invalid; ' +
              'it must be a function, usually from the `prop-types` package, but received `' + typeof typeSpecs[typeSpecName] + '`.'
            );
            err.name = 'Invariant Violation';
            throw err;
          }
          error = typeSpecs[typeSpecName](values, typeSpecName, componentName, location, null, ReactPropTypesSecret);
        } catch (ex) {
          error = ex;
        }
        if (error && !(error instanceof Error)) {
          printWarning(
            (componentName || 'React class') + ': type specification of ' +
            location + ' `' + typeSpecName + '` is invalid; the type checker ' +
            'function must return `null` or an `Error` but returned a ' + typeof error + '. ' +
            'You may have forgotten to pass an argument to the type checker ' +
            'creator (arrayOf, instanceOf, objectOf, oneOf, oneOfType, and ' +
            'shape all require an argument).'
          );
        }
        if (error instanceof Error && !(error.message in loggedTypeFailures)) {
          // Only monitor this failure once because there tends to be a lot of the
          // same error.
          loggedTypeFailures[error.message] = true;

          var stack = getStack ? getStack() : '';

          printWarning(
            'Failed ' + location + ' type: ' + error.message + (stack != null ? stack : '')
          );
        }
      }
    }
  }
}

/**
 * Resets warning cache when testing.
 *
 * @private
 */
checkPropTypes.resetWarningCache = function() {
  if (true) {
    loggedTypeFailures = {};
  }
}

module.exports = checkPropTypes;


/***/ }),

/***/ "./node_modules/prop-types/factoryWithTypeCheckers.js":
/*!************************************************************!*\
  !*** ./node_modules/prop-types/factoryWithTypeCheckers.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */



var ReactIs = __webpack_require__(/*! react-is */ "./node_modules/react-is/index.js");
var assign = __webpack_require__(/*! object-assign */ "./node_modules/object-assign/index.js");

var ReactPropTypesSecret = __webpack_require__(/*! ./lib/ReactPropTypesSecret */ "./node_modules/prop-types/lib/ReactPropTypesSecret.js");
var checkPropTypes = __webpack_require__(/*! ./checkPropTypes */ "./node_modules/prop-types/checkPropTypes.js");

var has = Function.call.bind(Object.prototype.hasOwnProperty);
var printWarning = function() {};

if (true) {
  printWarning = function(text) {
    var message = 'Warning: ' + text;
    if (typeof console !== 'undefined') {
      console.error(message);
    }
    try {
      // --- Welcome to debugging React ---
      // This error was thrown as a convenience so that you can use this stack
      // to find the callsite that caused this warning to fire.
      throw new Error(message);
    } catch (x) {}
  };
}

function emptyFunctionThatReturnsNull() {
  return null;
}

module.exports = function(isValidElement, throwOnDirectAccess) {
  /* global Symbol */
  var ITERATOR_SYMBOL = typeof Symbol === 'function' && Symbol.iterator;
  var FAUX_ITERATOR_SYMBOL = '@@iterator'; // Before Symbol spec.

  /**
   * Returns the iterator method function contained on the iterable object.
   *
   * Be sure to invoke the function with the iterable as context:
   *
   *     var iteratorFn = getIteratorFn(myIterable);
   *     if (iteratorFn) {
   *       var iterator = iteratorFn.call(myIterable);
   *       ...
   *     }
   *
   * @param {?object} maybeIterable
   * @return {?function}
   */
  function getIteratorFn(maybeIterable) {
    var iteratorFn = maybeIterable && (ITERATOR_SYMBOL && maybeIterable[ITERATOR_SYMBOL] || maybeIterable[FAUX_ITERATOR_SYMBOL]);
    if (typeof iteratorFn === 'function') {
      return iteratorFn;
    }
  }

  /**
   * Collection of methods that allow declaration and validation of props that are
   * supplied to React components. Example usage:
   *
   *   var Props = require('ReactPropTypes');
   *   var MyArticle = React.createClass({
   *     propTypes: {
   *       // An optional string prop named "description".
   *       description: Props.string,
   *
   *       // A required enum prop named "category".
   *       category: Props.oneOf(['News','Photos']).isRequired,
   *
   *       // A prop named "dialog" that requires an instance of Dialog.
   *       dialog: Props.instanceOf(Dialog).isRequired
   *     },
   *     render: function() { ... }
   *   });
   *
   * A more formal specification of how these methods are used:
   *
   *   type := array|bool|func|object|number|string|oneOf([...])|instanceOf(...)
   *   decl := ReactPropTypes.{type}(.isRequired)?
   *
   * Each and every declaration produces a function with the same signature. This
   * allows the creation of custom validation functions. For example:
   *
   *  var MyLink = React.createClass({
   *    propTypes: {
   *      // An optional string or URI prop named "href".
   *      href: function(props, propName, componentName) {
   *        var propValue = props[propName];
   *        if (propValue != null && typeof propValue !== 'string' &&
   *            !(propValue instanceof URI)) {
   *          return new Error(
   *            'Expected a string or an URI for ' + propName + ' in ' +
   *            componentName
   *          );
   *        }
   *      }
   *    },
   *    render: function() {...}
   *  });
   *
   * @internal
   */

  var ANONYMOUS = '<<anonymous>>';

  // Important!
  // Keep this list in sync with production version in `./factoryWithThrowingShims.js`.
  var ReactPropTypes = {
    array: createPrimitiveTypeChecker('array'),
    bool: createPrimitiveTypeChecker('boolean'),
    func: createPrimitiveTypeChecker('function'),
    number: createPrimitiveTypeChecker('number'),
    object: createPrimitiveTypeChecker('object'),
    string: createPrimitiveTypeChecker('string'),
    symbol: createPrimitiveTypeChecker('symbol'),

    any: createAnyTypeChecker(),
    arrayOf: createArrayOfTypeChecker,
    element: createElementTypeChecker(),
    elementType: createElementTypeTypeChecker(),
    instanceOf: createInstanceTypeChecker,
    node: createNodeChecker(),
    objectOf: createObjectOfTypeChecker,
    oneOf: createEnumTypeChecker,
    oneOfType: createUnionTypeChecker,
    shape: createShapeTypeChecker,
    exact: createStrictShapeTypeChecker,
  };

  /**
   * inlined Object.is polyfill to avoid requiring consumers ship their own
   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/is
   */
  /*eslint-disable no-self-compare*/
  function is(x, y) {
    // SameValue algorithm
    if (x === y) {
      // Steps 1-5, 7-10
      // Steps 6.b-6.e: +0 != -0
      return x !== 0 || 1 / x === 1 / y;
    } else {
      // Step 6.a: NaN == NaN
      return x !== x && y !== y;
    }
  }
  /*eslint-enable no-self-compare*/

  /**
   * We use an Error-like object for backward compatibility as people may call
   * PropTypes directly and inspect their output. However, we don't use real
   * Errors anymore. We don't inspect their stack anyway, and creating them
   * is prohibitively expensive if they are created too often, such as what
   * happens in oneOfType() for any type before the one that matched.
   */
  function PropTypeError(message) {
    this.message = message;
    this.stack = '';
  }
  // Make `instanceof Error` still work for returned errors.
  PropTypeError.prototype = Error.prototype;

  function createChainableTypeChecker(validate) {
    if (true) {
      var manualPropTypeCallCache = {};
      var manualPropTypeWarningCount = 0;
    }
    function checkType(isRequired, props, propName, componentName, location, propFullName, secret) {
      componentName = componentName || ANONYMOUS;
      propFullName = propFullName || propName;

      if (secret !== ReactPropTypesSecret) {
        if (throwOnDirectAccess) {
          // New behavior only for users of `prop-types` package
          var err = new Error(
            'Calling PropTypes validators directly is not supported by the `prop-types` package. ' +
            'Use `PropTypes.checkPropTypes()` to call them. ' +
            'Read more at http://fb.me/use-check-prop-types'
          );
          err.name = 'Invariant Violation';
          throw err;
        } else if ( true && typeof console !== 'undefined') {
          // Old behavior for people using React.PropTypes
          var cacheKey = componentName + ':' + propName;
          if (
            !manualPropTypeCallCache[cacheKey] &&
            // Avoid spamming the console because they are often not actionable except for lib authors
            manualPropTypeWarningCount < 3
          ) {
            printWarning(
              'You are manually calling a React.PropTypes validation ' +
              'function for the `' + propFullName + '` prop on `' + componentName  + '`. This is deprecated ' +
              'and will throw in the standalone `prop-types` package. ' +
              'You may be seeing this warning due to a third-party PropTypes ' +
              'library. See https://fb.me/react-warning-dont-call-proptypes ' + 'for details.'
            );
            manualPropTypeCallCache[cacheKey] = true;
            manualPropTypeWarningCount++;
          }
        }
      }
      if (props[propName] == null) {
        if (isRequired) {
          if (props[propName] === null) {
            return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required ' + ('in `' + componentName + '`, but its value is `null`.'));
          }
          return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required in ' + ('`' + componentName + '`, but its value is `undefined`.'));
        }
        return null;
      } else {
        return validate(props, propName, componentName, location, propFullName);
      }
    }

    var chainedCheckType = checkType.bind(null, false);
    chainedCheckType.isRequired = checkType.bind(null, true);

    return chainedCheckType;
  }

  function createPrimitiveTypeChecker(expectedType) {
    function validate(props, propName, componentName, location, propFullName, secret) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== expectedType) {
        // `propValue` being instance of, say, date/regexp, pass the 'object'
        // check, but we can offer a more precise error message here rather than
        // 'of type `object`'.
        var preciseType = getPreciseType(propValue);

        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + preciseType + '` supplied to `' + componentName + '`, expected ') + ('`' + expectedType + '`.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createAnyTypeChecker() {
    return createChainableTypeChecker(emptyFunctionThatReturnsNull);
  }

  function createArrayOfTypeChecker(typeChecker) {
    function validate(props, propName, componentName, location, propFullName) {
      if (typeof typeChecker !== 'function') {
        return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside arrayOf.');
      }
      var propValue = props[propName];
      if (!Array.isArray(propValue)) {
        var propType = getPropType(propValue);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an array.'));
      }
      for (var i = 0; i < propValue.length; i++) {
        var error = typeChecker(propValue, i, componentName, location, propFullName + '[' + i + ']', ReactPropTypesSecret);
        if (error instanceof Error) {
          return error;
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createElementTypeChecker() {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      if (!isValidElement(propValue)) {
        var propType = getPropType(propValue);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected a single ReactElement.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createElementTypeTypeChecker() {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      if (!ReactIs.isValidElementType(propValue)) {
        var propType = getPropType(propValue);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected a single ReactElement type.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createInstanceTypeChecker(expectedClass) {
    function validate(props, propName, componentName, location, propFullName) {
      if (!(props[propName] instanceof expectedClass)) {
        var expectedClassName = expectedClass.name || ANONYMOUS;
        var actualClassName = getClassName(props[propName]);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + actualClassName + '` supplied to `' + componentName + '`, expected ') + ('instance of `' + expectedClassName + '`.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createEnumTypeChecker(expectedValues) {
    if (!Array.isArray(expectedValues)) {
      if (true) {
        if (arguments.length > 1) {
          printWarning(
            'Invalid arguments supplied to oneOf, expected an array, got ' + arguments.length + ' arguments. ' +
            'A common mistake is to write oneOf(x, y, z) instead of oneOf([x, y, z]).'
          );
        } else {
          printWarning('Invalid argument supplied to oneOf, expected an array.');
        }
      }
      return emptyFunctionThatReturnsNull;
    }

    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      for (var i = 0; i < expectedValues.length; i++) {
        if (is(propValue, expectedValues[i])) {
          return null;
        }
      }

      var valuesString = JSON.stringify(expectedValues, function replacer(key, value) {
        var type = getPreciseType(value);
        if (type === 'symbol') {
          return String(value);
        }
        return value;
      });
      return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of value `' + String(propValue) + '` ' + ('supplied to `' + componentName + '`, expected one of ' + valuesString + '.'));
    }
    return createChainableTypeChecker(validate);
  }

  function createObjectOfTypeChecker(typeChecker) {
    function validate(props, propName, componentName, location, propFullName) {
      if (typeof typeChecker !== 'function') {
        return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside objectOf.');
      }
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an object.'));
      }
      for (var key in propValue) {
        if (has(propValue, key)) {
          var error = typeChecker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret);
          if (error instanceof Error) {
            return error;
          }
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createUnionTypeChecker(arrayOfTypeCheckers) {
    if (!Array.isArray(arrayOfTypeCheckers)) {
       true ? printWarning('Invalid argument supplied to oneOfType, expected an instance of array.') : undefined;
      return emptyFunctionThatReturnsNull;
    }

    for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
      var checker = arrayOfTypeCheckers[i];
      if (typeof checker !== 'function') {
        printWarning(
          'Invalid argument supplied to oneOfType. Expected an array of check functions, but ' +
          'received ' + getPostfixForTypeWarning(checker) + ' at index ' + i + '.'
        );
        return emptyFunctionThatReturnsNull;
      }
    }

    function validate(props, propName, componentName, location, propFullName) {
      for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
        var checker = arrayOfTypeCheckers[i];
        if (checker(props, propName, componentName, location, propFullName, ReactPropTypesSecret) == null) {
          return null;
        }
      }

      return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`.'));
    }
    return createChainableTypeChecker(validate);
  }

  function createNodeChecker() {
    function validate(props, propName, componentName, location, propFullName) {
      if (!isNode(props[propName])) {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`, expected a ReactNode.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createShapeTypeChecker(shapeTypes) {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
      }
      for (var key in shapeTypes) {
        var checker = shapeTypes[key];
        if (!checker) {
          continue;
        }
        var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret);
        if (error) {
          return error;
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createStrictShapeTypeChecker(shapeTypes) {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
      }
      // We need to check all keys in case some are required but missing from
      // props.
      var allKeys = assign({}, props[propName], shapeTypes);
      for (var key in allKeys) {
        var checker = shapeTypes[key];
        if (!checker) {
          return new PropTypeError(
            'Invalid ' + location + ' `' + propFullName + '` key `' + key + '` supplied to `' + componentName + '`.' +
            '\nBad object: ' + JSON.stringify(props[propName], null, '  ') +
            '\nValid keys: ' +  JSON.stringify(Object.keys(shapeTypes), null, '  ')
          );
        }
        var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret);
        if (error) {
          return error;
        }
      }
      return null;
    }

    return createChainableTypeChecker(validate);
  }

  function isNode(propValue) {
    switch (typeof propValue) {
      case 'number':
      case 'string':
      case 'undefined':
        return true;
      case 'boolean':
        return !propValue;
      case 'object':
        if (Array.isArray(propValue)) {
          return propValue.every(isNode);
        }
        if (propValue === null || isValidElement(propValue)) {
          return true;
        }

        var iteratorFn = getIteratorFn(propValue);
        if (iteratorFn) {
          var iterator = iteratorFn.call(propValue);
          var step;
          if (iteratorFn !== propValue.entries) {
            while (!(step = iterator.next()).done) {
              if (!isNode(step.value)) {
                return false;
              }
            }
          } else {
            // Iterator will provide entry [k,v] tuples rather than values.
            while (!(step = iterator.next()).done) {
              var entry = step.value;
              if (entry) {
                if (!isNode(entry[1])) {
                  return false;
                }
              }
            }
          }
        } else {
          return false;
        }

        return true;
      default:
        return false;
    }
  }

  function isSymbol(propType, propValue) {
    // Native Symbol.
    if (propType === 'symbol') {
      return true;
    }

    // falsy value can't be a Symbol
    if (!propValue) {
      return false;
    }

    // 19.4.3.5 Symbol.prototype[@@toStringTag] === 'Symbol'
    if (propValue['@@toStringTag'] === 'Symbol') {
      return true;
    }

    // Fallback for non-spec compliant Symbols which are polyfilled.
    if (typeof Symbol === 'function' && propValue instanceof Symbol) {
      return true;
    }

    return false;
  }

  // Equivalent of `typeof` but with special handling for array and regexp.
  function getPropType(propValue) {
    var propType = typeof propValue;
    if (Array.isArray(propValue)) {
      return 'array';
    }
    if (propValue instanceof RegExp) {
      // Old webkits (at least until Android 4.0) return 'function' rather than
      // 'object' for typeof a RegExp. We'll normalize this here so that /bla/
      // passes PropTypes.object.
      return 'object';
    }
    if (isSymbol(propType, propValue)) {
      return 'symbol';
    }
    return propType;
  }

  // This handles more types than `getPropType`. Only used for error messages.
  // See `createPrimitiveTypeChecker`.
  function getPreciseType(propValue) {
    if (typeof propValue === 'undefined' || propValue === null) {
      return '' + propValue;
    }
    var propType = getPropType(propValue);
    if (propType === 'object') {
      if (propValue instanceof Date) {
        return 'date';
      } else if (propValue instanceof RegExp) {
        return 'regexp';
      }
    }
    return propType;
  }

  // Returns a string that is postfixed to a warning about an invalid type.
  // For example, "undefined" or "of type array"
  function getPostfixForTypeWarning(value) {
    var type = getPreciseType(value);
    switch (type) {
      case 'array':
      case 'object':
        return 'an ' + type;
      case 'boolean':
      case 'date':
      case 'regexp':
        return 'a ' + type;
      default:
        return type;
    }
  }

  // Returns class name of the object, if any.
  function getClassName(propValue) {
    if (!propValue.constructor || !propValue.constructor.name) {
      return ANONYMOUS;
    }
    return propValue.constructor.name;
  }

  ReactPropTypes.checkPropTypes = checkPropTypes;
  ReactPropTypes.resetWarningCache = checkPropTypes.resetWarningCache;
  ReactPropTypes.PropTypes = ReactPropTypes;

  return ReactPropTypes;
};


/***/ }),

/***/ "./node_modules/prop-types/index.js":
/*!******************************************!*\
  !*** ./node_modules/prop-types/index.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

if (true) {
  var ReactIs = __webpack_require__(/*! react-is */ "./node_modules/react-is/index.js");

  // By explicitly using `prop-types` you are opting into new development behavior.
  // http://fb.me/prop-types-in-prod
  var throwOnDirectAccess = true;
  module.exports = __webpack_require__(/*! ./factoryWithTypeCheckers */ "./node_modules/prop-types/factoryWithTypeCheckers.js")(ReactIs.isElement, throwOnDirectAccess);
} else {}


/***/ }),

/***/ "./node_modules/prop-types/lib/ReactPropTypesSecret.js":
/*!*************************************************************!*\
  !*** ./node_modules/prop-types/lib/ReactPropTypesSecret.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */



var ReactPropTypesSecret = 'SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED';

module.exports = ReactPropTypesSecret;


/***/ }),

/***/ "./node_modules/react-is/cjs/react-is.development.js":
/*!***********************************************************!*\
  !*** ./node_modules/react-is/cjs/react-is.development.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/** @license React v16.9.0
 * react-is.development.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */





if (true) {
  (function() {
'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

// The Symbol used to tag the ReactElement-like types. If there is no native Symbol
// nor polyfill, then a plain number is used for performance.
var hasSymbol = typeof Symbol === 'function' && Symbol.for;

var REACT_ELEMENT_TYPE = hasSymbol ? Symbol.for('react.element') : 0xeac7;
var REACT_PORTAL_TYPE = hasSymbol ? Symbol.for('react.portal') : 0xeaca;
var REACT_FRAGMENT_TYPE = hasSymbol ? Symbol.for('react.fragment') : 0xeacb;
var REACT_STRICT_MODE_TYPE = hasSymbol ? Symbol.for('react.strict_mode') : 0xeacc;
var REACT_PROFILER_TYPE = hasSymbol ? Symbol.for('react.profiler') : 0xead2;
var REACT_PROVIDER_TYPE = hasSymbol ? Symbol.for('react.provider') : 0xeacd;
var REACT_CONTEXT_TYPE = hasSymbol ? Symbol.for('react.context') : 0xeace;
// TODO: We don't use AsyncMode or ConcurrentMode anymore. They were temporary
// (unstable) APIs that have been removed. Can we remove the symbols?
var REACT_ASYNC_MODE_TYPE = hasSymbol ? Symbol.for('react.async_mode') : 0xeacf;
var REACT_CONCURRENT_MODE_TYPE = hasSymbol ? Symbol.for('react.concurrent_mode') : 0xeacf;
var REACT_FORWARD_REF_TYPE = hasSymbol ? Symbol.for('react.forward_ref') : 0xead0;
var REACT_SUSPENSE_TYPE = hasSymbol ? Symbol.for('react.suspense') : 0xead1;
var REACT_SUSPENSE_LIST_TYPE = hasSymbol ? Symbol.for('react.suspense_list') : 0xead8;
var REACT_MEMO_TYPE = hasSymbol ? Symbol.for('react.memo') : 0xead3;
var REACT_LAZY_TYPE = hasSymbol ? Symbol.for('react.lazy') : 0xead4;
var REACT_FUNDAMENTAL_TYPE = hasSymbol ? Symbol.for('react.fundamental') : 0xead5;
var REACT_RESPONDER_TYPE = hasSymbol ? Symbol.for('react.responder') : 0xead6;

function isValidElementType(type) {
  return typeof type === 'string' || typeof type === 'function' ||
  // Note: its typeof might be other than 'symbol' or 'number' if it's a polyfill.
  type === REACT_FRAGMENT_TYPE || type === REACT_CONCURRENT_MODE_TYPE || type === REACT_PROFILER_TYPE || type === REACT_STRICT_MODE_TYPE || type === REACT_SUSPENSE_TYPE || type === REACT_SUSPENSE_LIST_TYPE || typeof type === 'object' && type !== null && (type.$$typeof === REACT_LAZY_TYPE || type.$$typeof === REACT_MEMO_TYPE || type.$$typeof === REACT_PROVIDER_TYPE || type.$$typeof === REACT_CONTEXT_TYPE || type.$$typeof === REACT_FORWARD_REF_TYPE || type.$$typeof === REACT_FUNDAMENTAL_TYPE || type.$$typeof === REACT_RESPONDER_TYPE);
}

/**
 * Forked from fbjs/warning:
 * https://github.com/facebook/fbjs/blob/e66ba20ad5be433eb54423f2b097d829324d9de6/packages/fbjs/src/__forks__/warning.js
 *
 * Only change is we use console.warn instead of console.error,
 * and do nothing when 'console' is not supported.
 * This really simplifies the code.
 * ---
 * Similar to invariant but only logs a warning if the condition is not met.
 * This can be used to log issues in development environments in critical
 * paths. Removing the logging code for production environments will keep the
 * same logic and follow the same code paths.
 */

var lowPriorityWarning = function () {};

{
  var printWarning = function (format) {
    for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    var argIndex = 0;
    var message = 'Warning: ' + format.replace(/%s/g, function () {
      return args[argIndex++];
    });
    if (typeof console !== 'undefined') {
      console.warn(message);
    }
    try {
      // --- Welcome to debugging React ---
      // This error was thrown as a convenience so that you can use this stack
      // to find the callsite that caused this warning to fire.
      throw new Error(message);
    } catch (x) {}
  };

  lowPriorityWarning = function (condition, format) {
    if (format === undefined) {
      throw new Error('`lowPriorityWarning(condition, format, ...args)` requires a warning ' + 'message argument');
    }
    if (!condition) {
      for (var _len2 = arguments.length, args = Array(_len2 > 2 ? _len2 - 2 : 0), _key2 = 2; _key2 < _len2; _key2++) {
        args[_key2 - 2] = arguments[_key2];
      }

      printWarning.apply(undefined, [format].concat(args));
    }
  };
}

var lowPriorityWarning$1 = lowPriorityWarning;

function typeOf(object) {
  if (typeof object === 'object' && object !== null) {
    var $$typeof = object.$$typeof;
    switch ($$typeof) {
      case REACT_ELEMENT_TYPE:
        var type = object.type;

        switch (type) {
          case REACT_ASYNC_MODE_TYPE:
          case REACT_CONCURRENT_MODE_TYPE:
          case REACT_FRAGMENT_TYPE:
          case REACT_PROFILER_TYPE:
          case REACT_STRICT_MODE_TYPE:
          case REACT_SUSPENSE_TYPE:
            return type;
          default:
            var $$typeofType = type && type.$$typeof;

            switch ($$typeofType) {
              case REACT_CONTEXT_TYPE:
              case REACT_FORWARD_REF_TYPE:
              case REACT_PROVIDER_TYPE:
                return $$typeofType;
              default:
                return $$typeof;
            }
        }
      case REACT_LAZY_TYPE:
      case REACT_MEMO_TYPE:
      case REACT_PORTAL_TYPE:
        return $$typeof;
    }
  }

  return undefined;
}

// AsyncMode is deprecated along with isAsyncMode
var AsyncMode = REACT_ASYNC_MODE_TYPE;
var ConcurrentMode = REACT_CONCURRENT_MODE_TYPE;
var ContextConsumer = REACT_CONTEXT_TYPE;
var ContextProvider = REACT_PROVIDER_TYPE;
var Element = REACT_ELEMENT_TYPE;
var ForwardRef = REACT_FORWARD_REF_TYPE;
var Fragment = REACT_FRAGMENT_TYPE;
var Lazy = REACT_LAZY_TYPE;
var Memo = REACT_MEMO_TYPE;
var Portal = REACT_PORTAL_TYPE;
var Profiler = REACT_PROFILER_TYPE;
var StrictMode = REACT_STRICT_MODE_TYPE;
var Suspense = REACT_SUSPENSE_TYPE;

var hasWarnedAboutDeprecatedIsAsyncMode = false;

// AsyncMode should be deprecated
function isAsyncMode(object) {
  {
    if (!hasWarnedAboutDeprecatedIsAsyncMode) {
      hasWarnedAboutDeprecatedIsAsyncMode = true;
      lowPriorityWarning$1(false, 'The ReactIs.isAsyncMode() alias has been deprecated, ' + 'and will be removed in React 17+. Update your code to use ' + 'ReactIs.isConcurrentMode() instead. It has the exact same API.');
    }
  }
  return isConcurrentMode(object) || typeOf(object) === REACT_ASYNC_MODE_TYPE;
}
function isConcurrentMode(object) {
  return typeOf(object) === REACT_CONCURRENT_MODE_TYPE;
}
function isContextConsumer(object) {
  return typeOf(object) === REACT_CONTEXT_TYPE;
}
function isContextProvider(object) {
  return typeOf(object) === REACT_PROVIDER_TYPE;
}
function isElement(object) {
  return typeof object === 'object' && object !== null && object.$$typeof === REACT_ELEMENT_TYPE;
}
function isForwardRef(object) {
  return typeOf(object) === REACT_FORWARD_REF_TYPE;
}
function isFragment(object) {
  return typeOf(object) === REACT_FRAGMENT_TYPE;
}
function isLazy(object) {
  return typeOf(object) === REACT_LAZY_TYPE;
}
function isMemo(object) {
  return typeOf(object) === REACT_MEMO_TYPE;
}
function isPortal(object) {
  return typeOf(object) === REACT_PORTAL_TYPE;
}
function isProfiler(object) {
  return typeOf(object) === REACT_PROFILER_TYPE;
}
function isStrictMode(object) {
  return typeOf(object) === REACT_STRICT_MODE_TYPE;
}
function isSuspense(object) {
  return typeOf(object) === REACT_SUSPENSE_TYPE;
}

exports.typeOf = typeOf;
exports.AsyncMode = AsyncMode;
exports.ConcurrentMode = ConcurrentMode;
exports.ContextConsumer = ContextConsumer;
exports.ContextProvider = ContextProvider;
exports.Element = Element;
exports.ForwardRef = ForwardRef;
exports.Fragment = Fragment;
exports.Lazy = Lazy;
exports.Memo = Memo;
exports.Portal = Portal;
exports.Profiler = Profiler;
exports.StrictMode = StrictMode;
exports.Suspense = Suspense;
exports.isValidElementType = isValidElementType;
exports.isAsyncMode = isAsyncMode;
exports.isConcurrentMode = isConcurrentMode;
exports.isContextConsumer = isContextConsumer;
exports.isContextProvider = isContextProvider;
exports.isElement = isElement;
exports.isForwardRef = isForwardRef;
exports.isFragment = isFragment;
exports.isLazy = isLazy;
exports.isMemo = isMemo;
exports.isPortal = isPortal;
exports.isProfiler = isProfiler;
exports.isStrictMode = isStrictMode;
exports.isSuspense = isSuspense;
  })();
}


/***/ }),

/***/ "./node_modules/react-is/index.js":
/*!****************************************!*\
  !*** ./node_modules/react-is/index.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


if (false) {} else {
  module.exports = __webpack_require__(/*! ./cjs/react-is.development.js */ "./node_modules/react-is/cjs/react-is.development.js");
}


/***/ }),

/***/ "./src/components/Container.js":
/*!*************************************!*\
  !*** ./src/components/Container.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../store */ "./src/store/index.js");
/* harmony import */ var _Notification__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Notification */ "./src/components/Notification.js");
/* harmony import */ var _utils_helpers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../utils/helpers */ "./src/utils/helpers.js");
/* harmony import */ var _scss_notification_scss__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../scss/notification.scss */ "./src/scss/notification.scss");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }








var Container = /*#__PURE__*/function (_React$Component) {
  _inherits(Container, _React$Component);

  var _super = _createSuper(Container);

  function Container(props) {
    var _this;

    _classCallCheck(this, Container);

    _this = _super.call(this, props);
    _this.state = {
      isMobile: props.isMobile,
      breakpoint: props.breakpoint,
      notifications: []
    };
    _this.add = _this.add.bind(_assertThisInitialized(_this));
    _this.remove = _this.remove.bind(_assertThisInitialized(_this));
    _this.toggleRemoval = _this.toggleRemoval.bind(_assertThisInitialized(_this));
    _this.handleResize = _this.handleResize.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(Container, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var types = this.props.types;
      _store__WEBPACK_IMPORTED_MODULE_2__["default"].register({
        addNotification: this.add,
        removeNotification: this.remove,
        types: types
      });
      this.setState({
        width: window.innerWidth
      });
      window.addEventListener('resize', this.handleResize);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      window.removeEventListener('resize', this.handleResize);
    }
  }, {
    key: "handleResize",
    value: function handleResize() {
      this.setState({
        width: window.innerWidth
      });
    }
  }, {
    key: "add",
    value: function add(notification) {
      this.setState(function (_ref) {
        var notifications = _ref.notifications;
        return {
          notifications: notification.insert === 'top' ? [notification].concat(_toConsumableArray(notifications)) : [].concat(_toConsumableArray(notifications), [notification])
        };
      });
      return notification.id;
    }
  }, {
    key: "remove",
    value: function remove(id) {
      this.setState(function (_ref2) {
        var notifications = _ref2.notifications;
        return {
          notifications: notifications.map(function (notification) {
            if (notification.id === id) {
              notification.removed = true;
            }

            return notification;
          })
        };
      });
    }
  }, {
    key: "toggleRemoval",
    value: function toggleRemoval(id, callback) {
      this.setState(function (_ref3) {
        var notifications = _ref3.notifications;
        return {
          notifications: notifications.filter(function (_ref4) {
            var nId = _ref4.id;
            return nId !== id;
          })
        };
      }, callback);
    }
  }, {
    key: "renderNotifications",
    value: function renderNotifications(notifications) {
      var _this2 = this;

      return notifications.map(function (notification) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Notification__WEBPACK_IMPORTED_MODULE_3__["default"], {
          id: notification.id,
          key: notification.id,
          notification: notification,
          toggleRemoval: _this2.toggleRemoval,
          count: notifications.length,
          removed: notification.removed
        });
      });
    }
  }, {
    key: "renderMobileNotifications",
    value: function renderMobileNotifications(props) {
      var className = props.className,
          id = props.id;
      var notifications = this.state.notifications;
      var mobileNotifications = Object(_utils_helpers__WEBPACK_IMPORTED_MODULE_4__["getNotificationsForMobileView"])(notifications);
      var top = this.renderNotifications(mobileNotifications.top);
      var bottom = this.renderNotifications(mobileNotifications.bottom);
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        id: id,
        key: "mobile",
        className: "react-notification-root ".concat(className || '')
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "notification-container-mobile-top"
      }, top), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "notification-container-mobile-bottom"
      }, bottom));
    }
  }, {
    key: "renderScreenNotifications",
    value: function renderScreenNotifications(props) {
      var className = props.className,
          id = props.id;
      var notifications = this.state.notifications;
      var items = Object(_utils_helpers__WEBPACK_IMPORTED_MODULE_4__["getNotificationsForEachContainer"])(notifications);
      var topLeft = this.renderNotifications(items.topLeft);
      var topRight = this.renderNotifications(items.topRight);
      var topCenter = this.renderNotifications(items.topCenter);
      var bottomLeft = this.renderNotifications(items.bottomLeft);
      var bottomRight = this.renderNotifications(items.bottomRight);
      var bottomCenter = this.renderNotifications(items.bottomCenter);
      var center = this.renderNotifications(items.center);
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        id: id,
        key: "screen",
        className: "react-notification-root ".concat(className || '')
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "notification-container-top-left"
      }, topLeft), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "notification-container-top-right"
      }, topRight), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "notification-container-bottom-left"
      }, bottomLeft), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "notification-container-bottom-right"
      }, bottomRight), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "notification-container-top-center"
      }, topCenter), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "notification-container-center"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "center-inner"
      }, center)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "notification-container-bottom-center"
      }, bottomCenter));
    }
  }, {
    key: "render",
    value: function render() {
      var isMobile = this.props.isMobile;
      var _this$state = this.state,
          width = _this$state.width,
          breakpoint = _this$state.breakpoint;

      if (isMobile && width <= breakpoint) {
        return this.renderMobileNotifications(this.props);
      }

      return this.renderScreenNotifications(this.props);
    }
  }]);

  return Container;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

Container.propTypes = {
  isMobile: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  breakpoint: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.number,
  types: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.array
};
Container.defaultProps = {
  isMobile: true,
  breakpoint: 768
};
/* harmony default export */ __webpack_exports__["default"] = (Container);

/***/ }),

/***/ "./src/components/Notification.js":
/*!****************************************!*\
  !*** ./src/components/Notification.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils_timer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/timer */ "./src/utils/timer.js");
/* harmony import */ var _utils_helpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../utils/helpers */ "./src/utils/helpers.js");
/* harmony import */ var _utils_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../utils/constants */ "./src/utils/constants.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }







var Notification = /*#__PURE__*/function (_React$Component) {
  _inherits(Notification, _React$Component);

  var _super = _createSuper(Notification);

  function Notification(props) {
    var _this;

    _classCallCheck(this, Notification);

    _this = _super.call(this, props);
    _this.rootElementRef = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createRef();
    _this.onClick = _this.onClick.bind(_assertThisInitialized(_this));
    _this.onTouchStart = _this.onTouchStart.bind(_assertThisInitialized(_this));
    _this.onTouchMove = _this.onTouchMove.bind(_assertThisInitialized(_this));
    _this.onTouchEnd = _this.onTouchEnd.bind(_assertThisInitialized(_this));
    _this.onMouseEnter = _this.onMouseEnter.bind(_assertThisInitialized(_this));
    _this.onMouseLeave = _this.onMouseLeave.bind(_assertThisInitialized(_this));
    var width = props.notification.width;
    _this.state = {
      parentStyle: {
        height: 0,
        overflow: 'hidden',
        width: width ? "".concat(width, "px") : '100%'
      },
      htmlClassList: Object(_utils_helpers__WEBPACK_IMPORTED_MODULE_3__["getHtmlClassesForType"])(props.notification),
      animationPlayState: 'running',
      touchEnabled: true
    };
    return _this;
  }

  _createClass(Notification, [{
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.timer) {
        this.timer.clear();
      }
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var _this$props = this.props,
          notification = _this$props.notification,
          count = _this$props.count;
      var _notification$dismiss = notification.dismiss,
          duration = _notification$dismiss.duration,
          onScreen = _notification$dismiss.onScreen;
      var scrollHeight = this.rootElementRef.current.scrollHeight;
      var willSlide = Object(_utils_helpers__WEBPACK_IMPORTED_MODULE_3__["shouldNotificationHaveSliding"])(notification, count);

      var onTransitionEnd = function onTransitionEnd() {
        if (!duration || onScreen) return;

        var callback = function callback() {
          return _this2.removeNotification(_utils_constants__WEBPACK_IMPORTED_MODULE_4__["REMOVAL"].TIMEOUT);
        };

        _this2.timer = new _utils_timer__WEBPACK_IMPORTED_MODULE_2__["default"](callback, duration);
      };

      var callback = function callback() {
        requestAnimationFrame(function () {
          _this2.setState(function (prevState) {
            return {
              htmlClassList: [].concat(_toConsumableArray(notification.animationIn), _toConsumableArray(prevState.htmlClassList))
            };
          });
        });
      };

      this.setState(function (_ref) {
        var width = _ref.parentStyle.width;
        return {
          parentStyle: {
            width: width,
            height: "".concat(scrollHeight, "px"),
            transition: willSlide ? Object(_utils_helpers__WEBPACK_IMPORTED_MODULE_3__["getTransition"])(notification.slidingEnter, 'height') : '10ms height'
          },
          onTransitionEnd: onTransitionEnd
        };
      }, callback);
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(_ref2) {
      var removed = _ref2.removed;

      if (this.props.removed && !removed) {
        this.removeNotification(_utils_constants__WEBPACK_IMPORTED_MODULE_4__["REMOVAL"].MANUAL);
      }
    }
  }, {
    key: "removeNotification",
    value: function removeNotification(removalFlag) {
      var _this3 = this;

      var _this$props2 = this.props,
          notification = _this$props2.notification,
          toggleRemoval = _this$props2.toggleRemoval;
      var id = notification.id,
          onRemoval = notification.onRemoval,
          waitForAnimation = notification.dismiss.waitForAnimation;
      var htmlClassList = [].concat(_toConsumableArray(notification.animationOut), _toConsumableArray(Object(_utils_helpers__WEBPACK_IMPORTED_MODULE_3__["getHtmlClassesForType"])(notification)));

      var onTransitionEnd = function onTransitionEnd() {
        return toggleRemoval(id, function () {
          return onRemoval(id, removalFlag);
        });
      };

      var parentStyle = {
        height: 0,
        transition: Object(_utils_helpers__WEBPACK_IMPORTED_MODULE_3__["getTransition"])(notification.slidingExit, 'height')
      };

      if (waitForAnimation) {
        return this.setState(function (_ref3) {
          var width = _ref3.parentStyle.width;
          return {
            htmlClassList: htmlClassList,
            onAnimationEnd: function onAnimationEnd() {
              _this3.setState({
                parentStyle: _objectSpread({
                  width: width
                }, parentStyle),
                onTransitionEnd: onTransitionEnd
              });
            }
          };
        });
      }

      return this.setState(function (_ref4) {
        var width = _ref4.parentStyle.width;
        return {
          parentStyle: _objectSpread({
            width: width
          }, parentStyle),
          onTransitionEnd: onTransitionEnd,
          htmlClassList: htmlClassList
        };
      });
    }
  }, {
    key: "onClick",
    value: function onClick() {
      var dismiss = this.props.notification.dismiss;

      if (dismiss.click || dismiss.showIcon) {
        this.removeNotification(_utils_constants__WEBPACK_IMPORTED_MODULE_4__["REMOVAL"].CLICK);
      }
    }
  }, {
    key: "onTouchStart",
    value: function onTouchStart(_ref5) {
      var touches = _ref5.touches;

      var _touches = _slicedToArray(touches, 1),
          pageX = _touches[0].pageX;

      this.setState(function (_ref6) {
        var parentStyle = _ref6.parentStyle;
        return {
          startX: pageX,
          currentX: pageX,
          parentStyle: _objectSpread(_objectSpread({}, parentStyle), {}, {
            position: 'relative'
          })
        };
      });
    }
  }, {
    key: "onTouchMove",
    value: function onTouchMove(_ref7) {
      var _this4 = this;

      var touches = _ref7.touches;
      var startX = this.state.startX;
      var _this$props3 = this.props,
          toggleRemoval = _this$props3.toggleRemoval,
          _this$props3$notifica = _this$props3.notification,
          id = _this$props3$notifica.id,
          onRemoval = _this$props3$notifica.onRemoval,
          slidingExit = _this$props3$notifica.slidingExit,
          _this$props3$notifica2 = _this$props3$notifica.touchSlidingExit,
          swipe = _this$props3$notifica2.swipe,
          fade = _this$props3$notifica2.fade;

      var _touches2 = _slicedToArray(touches, 1),
          pageX = _touches2[0].pageX;

      var distance = pageX - startX;
      var width = this.rootElementRef.current.offsetWidth;
      var swipeTo = window.innerWidth + width;
      var left = "".concat(pageX - startX >= 0 ? swipeTo : -swipeTo, "px");

      if (Object(_utils_helpers__WEBPACK_IMPORTED_MODULE_3__["hasFullySwiped"])(distance, width)) {
        var t1 = Object(_utils_helpers__WEBPACK_IMPORTED_MODULE_3__["getTransition"])(swipe, 'left');
        var t2 = Object(_utils_helpers__WEBPACK_IMPORTED_MODULE_3__["getTransition"])(fade, 'opacity');

        var _onTransitionEnd = function onTransitionEnd() {
          toggleRemoval(id, function () {
            return onRemoval(id, _utils_constants__WEBPACK_IMPORTED_MODULE_4__["REMOVAL"].TOUCH);
          });
        };

        return this.setState(function (_ref8) {
          var parentStyle = _ref8.parentStyle;
          return {
            touchEnabled: false,
            parentStyle: _objectSpread(_objectSpread({}, parentStyle), {}, {
              left: left,
              opacity: 0,
              transition: "".concat(t1, ", ").concat(t2)
            }),
            onTransitionEnd: function onTransitionEnd() {
              _this4.setState(function (_ref9) {
                var parentStyle = _ref9.parentStyle;
                return {
                  parentStyle: _objectSpread(_objectSpread({}, parentStyle), {}, {
                    height: 0,
                    transition: Object(_utils_helpers__WEBPACK_IMPORTED_MODULE_3__["getTransition"])(slidingExit, 'height')
                  }),
                  onTransitionEnd: _onTransitionEnd
                };
              });
            }
          };
        });
      }

      return this.setState(function (_ref10) {
        var parentStyle = _ref10.parentStyle;
        return {
          currentX: pageX,
          parentStyle: _objectSpread(_objectSpread({}, parentStyle), {}, {
            left: "".concat(0 + distance, "px")
          })
        };
      });
    }
  }, {
    key: "onTouchEnd",
    value: function onTouchEnd() {
      var touchRevert = this.props.notification.touchRevert;
      this.setState(function (_ref11) {
        var parentStyle = _ref11.parentStyle;
        return {
          parentStyle: _objectSpread(_objectSpread({}, parentStyle), {}, {
            left: 0,
            transition: Object(_utils_helpers__WEBPACK_IMPORTED_MODULE_3__["getTransition"])(touchRevert, 'left')
          })
        };
      });
    }
  }, {
    key: "onMouseEnter",
    value: function onMouseEnter() {
      if (this.timer) {
        this.timer.pause();
      } else {
        this.setState({
          animationPlayState: 'paused'
        });
      }
    }
  }, {
    key: "onMouseLeave",
    value: function onMouseLeave() {
      if (this.timer) {
        this.timer.resume();
      } else {
        this.setState({
          animationPlayState: 'running'
        });
      }
    }
  }, {
    key: "renderTimer",
    value: function renderTimer() {
      var _this5 = this;

      var dismiss = this.props.notification.dismiss;
      var duration = dismiss.duration,
          onScreen = dismiss.onScreen;
      var animationPlayState = this.state.animationPlayState;
      if (!duration || !onScreen) return;
      var style = {
        animationName: 'timer',
        animationDuration: "".concat(duration, "ms"),
        animationTimingFunction: 'linear',
        animationFillMode: 'forwards',
        animationDelay: 0,
        animationPlayState: animationPlayState
      };

      var onAnimationEnd = function onAnimationEnd() {
        return _this5.removeNotification(_utils_constants__WEBPACK_IMPORTED_MODULE_4__["REMOVAL"].TIMEOUT);
      };

      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: ""
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "",
        onAnimationEnd: onAnimationEnd,
        style: style
      }));
    }
  }, {
    key: "renderCustomContent",
    value: function renderCustomContent() {
      var htmlClassList = this.state.htmlClassList;
      var _this$props$notificat = this.props.notification,
          id = _this$props$notificat.id,
          CustomContent = _this$props$notificat.content,
          _this$props$notificat2 = _this$props$notificat.dismiss,
          duration = _this$props$notificat2.duration,
          pauseOnHover = _this$props$notificat2.pauseOnHover;
      var hasMouseEvents = duration > 0 && pauseOnHover;

      
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat([].concat(_toConsumableArray(htmlClassList), ['n-child']).join(' ')),
        onMouseEnter: hasMouseEvents ? this.onMouseEnter : null,
        onMouseLeave: hasMouseEvents ? this.onMouseLeave : null
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.isValidElement(CustomContent) ? CustomContent : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(CustomContent, {
        id: id
      }));
    }
  }, {
    key: "renderNotification",
    value: function renderNotification() {
      var _this$props$notificat3 = this.props.notification,
          title = _this$props$notificat3.title,
          titleIcon = _this$props$notificat3.titleIcon,
          message = _this$props$notificat3.message,
          _this$props$notificat4 = _this$props$notificat3.dismiss,
          showIcon = _this$props$notificat4.showIcon,
          duration = _this$props$notificat4.duration,
          pauseOnHover = _this$props$notificat4.pauseOnHover;
      var htmlClassList = this.state.htmlClassList;
      var hasMouseEvents = duration > 0 && pauseOnHover;
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "".concat([].concat(_toConsumableArray(htmlClassList), ['n-child']).join(' ')),
        onMouseEnter: hasMouseEvents ? this.onMouseEnter : null,
        onMouseLeave: hasMouseEvents ? this.onMouseLeave : null
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "notification-content"
      }, showIcon && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "notification-close",
        onClick: this.onClick
      }), title && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "notification-title"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: titleIcon,
        className:"titleIcon"
      }),title),
       /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "notification-message"
      }, message), this.renderTimer()));
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props$notificat5 = this.props.notification,
          content = _this$props$notificat5.content,
          click = _this$props$notificat5.dismiss.click;
      var _this$state = this.state,
          parentStyle = _this$state.parentStyle,
          onAnimationEnd = _this$state.onAnimationEnd,
          onTransitionEnd = _this$state.onTransitionEnd,
          touchEnabled = _this$state.touchEnabled;
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        ref: this.rootElementRef,
        onClick: click ? this.onClick : null,
        className: "notification-parent",
        style: parentStyle,
        onAnimationEnd: onAnimationEnd,
        onTransitionEnd: onTransitionEnd,
        onTouchStart: touchEnabled ? this.onTouchStart : null,
        onTouchMove: touchEnabled ? this.onTouchMove : null,
        onTouchEnd: touchEnabled ? this.onTouchEnd : null
      }, content ? this.renderCustomContent() : this.renderNotification());
    }
  }]);

  return Notification;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

Notification.propTypes = {
  toggleRemoval: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  count: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.number.isRequired,
  removed: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool
};
/* harmony default export */ __webpack_exports__["default"] = (Notification);

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! exports provided: store, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./store */ "./src/store/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "store", function() { return _store__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony import */ var _components_Container__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/Container */ "./src/components/Container.js");



/* harmony default export */ __webpack_exports__["default"] = (_components_Container__WEBPACK_IMPORTED_MODULE_1__["default"]);

/***/ }),

/***/ "./src/scss/notification.scss":
/*!************************************!*\
  !*** ./src/scss/notification.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(true);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".notification-container-top-center,\n.notification-container-top-left,\n.notification-container-top-right,\n.notification-container-bottom-center,\n.notification-container-bottom-left,\n.notification-container-bottom-right,\n.notification-container-center {\n  width: 325px;\n  position: absolute;\n  pointer-events: all; }\n\n.notification-container-center,\n.notification-container-top-center,\n.notification-container-bottom-center {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column;\n  left: calc(50% - 162.5px); }\n\n.notification-container-center {\n  top: 20px;\n  height: 100%;\n  pointer-events: none; }\n  .notification-container-center .center-inner {\n    width: 325px;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    flex-direction: column;\n    pointer-events: all; }\n\n.notification-container-top-center {\n  top: 20px; }\n\n.notification-container-bottom-center {\n  bottom: 20px; }\n\n.notification-container-top-left {\n  left: 20px;\n  top: 20px; }\n\n.notification-container-top-right {\n  right: 20px;\n  top: 20px; }\n\n.notification-container-bottom-left {\n  left: 20px;\n  bottom: 20px; }\n\n.notification-container-bottom-right {\n  bottom: 20px;\n  right: 20px; }\n\n.notification-container-mobile-top,\n.notification-container-mobile-bottom {\n  pointer-events: all;\n  position: absolute; }\n\n.notification-container-mobile-top {\n  right: 20px;\n  left: 20px;\n  top: 20px; }\n\n.notification-container-mobile-bottom {\n  right: 20px;\n  left: 20px;\n  bottom: 20px;\n  margin-bottom: -15px; }\n\n.notification-default {\n  background-color: #007bff;\n  border-left: 8px solid #0562c7; }\n  .notification-default .timer {\n    background-color: #007bff; }\n  .notification-default .timer-filler {\n    background-color: #fff; }\n  .notification-default .notification-close {\n    background-color: #007bff; }\n\n.notification-success {\n  background-color: #28a745;\n  border-left: 8px solid #1f8838; }\n  .notification-success .timer {\n    background-color: #28a745; }\n  .notification-success .timer-filler {\n    background-color: #fff; }\n  .notification-success .notification-close {\n    background-color: #28a745; }\n\n.notification-danger {\n  background-color: #dc3545;\n  border-left: 8px solid #bd1120; }\n  .notification-danger .timer {\n    background-color: #dc3545; }\n  .notification-danger .timer-filler {\n    background-color: #fff; }\n  .notification-danger .notification-close {\n    background-color: #dc3545; }\n\n.notification-info {\n  background-color: #17a2b8;\n  border-left: 8px solid #138b9e; }\n  .notification-info .timer {\n    background-color: #17a2b8; }\n  .notification-info .timer-filler {\n    background-color: #fff; }\n  .notification-info .notification-close {\n    background-color: #17a2b8; }\n\n.notification-warning {\n  background-color: #eab000;\n  border-left: 8px solid #ce9c09; }\n  .notification-warning .timer {\n    background-color: #eab000; }\n  .notification-warning .timer-filler {\n    background-color: #fff; }\n  .notification-warning .notification-close {\n    background-color: #eab000; }\n\n.notification-awesome {\n  background-color: #685dc3;\n  border-left: 8px solid #4c3fb1; }\n  .notification-awesome .timer {\n    background-color: #685dc3; }\n  .notification-awesome .timer-filler {\n    background-color: #fff; }\n  .notification-awesome .notification-close {\n    background-color: #685dc3; }\n\n@keyframes timer {\n  0% {\n    width: 100%; }\n  100% {\n    width: 0%; } }\n\n.react-notification-root {\n  position: fixed;\n  z-index: 9000;\n  pointer-events: none;\n  width: 100%;\n  height: 100%; }\n\n.notification-item {\n  display: flex;\n  position: relative;\n  border-radius: 3px;\n  margin-bottom: 15px;\n  box-shadow: 1px 3px 4px rgba(0, 0, 0, 0.2);\n  cursor: pointer; }\n  .notification-item .timer {\n    width: 100%;\n    height: 3px;\n    margin-top: 10px;\n    border-radius: 5px; }\n    .notification-item .timer .timer-filler {\n      height: 3px;\n      border-radius: 5px; }\n  .notification-item .notification-title {\n    color: #fff;\n    font-weight: 700;\n    font-size: 14px;\n    margin-top: 5px;\n    margin-bottom: 5px; }\n  .notification-item .notification-message {\n    color: #fff;\n    max-width: calc(100% - 15px);\n    font-size: 14px;\n    line-height: 150%;\n    word-wrap: break-word;\n    margin-bottom: 0;\n    margin-top: 0; }\n  .notification-item .notification-content {\n    padding: 8px 15px;\n    display: inline-block;\n    width: 100%; }\n  .notification-item .notification-close {\n    width: 18px;\n    height: 18px;\n    border-radius: 50%;\n    display: inline-block;\n    position: absolute;\n    right: 10px;\n    top: 10px; }\n    .notification-item .notification-close::after {\n      content: '\\D7';\n      position: absolute;\n      transform: translate(-50%, -50%);\n      color: #fff;\n      font-size: 12px;\n      left: 50%;\n      top: 50%; }\n\n.notification-container-mobile-top .notification-item,\n.notification-container-mobile-bottom .notification-item,\n.notification-container-mobile-top .notification-parent,\n.notification-container-mobile-bottom .notification-parent {\n  max-width: 100%;\n  width: 100%; }\n\n.notification-container-top-right .notification-parent,\n.notification-container-bottom-right .notification-parent {\n  margin-left: auto; }\n\n.notification-container-top-left .notification-parent,\n.notification-container-bottom-left .notification-parent {\n  margin-right: auto; }\n\n.notification-container-mobile-top .notification-parent,\n.notification-container-mobile-bottom .notification-parent {\n  margin-left: auto;\n  margin-right: auto; }\n", "",{"version":3,"sources":["F:/Projects/react-notifications-component/src/scss/_containers.scss","F:/Projects/react-notifications-component/src/scss/_types.scss","F:/Projects/react-notifications-component/src/scss/_variables.scss","F:/Projects/react-notifications-component/src/scss/notification.scss"],"names":[],"mappings":"AAAA;;;;;;;EAOE,YAAY;EACZ,kBAAkB;EAClB,mBAAmB,EAAA;;AAGrB;;;EAGE,aAAa;EACb,uBAAuB;EACvB,mBAAmB;EACnB,sBAAsB;EACtB,yBAAyB,EAAA;;AAG3B;EACE,SAAS;EACT,YAAY;EACZ,oBAAoB,EAAA;EAHtB;IAMI,YAAY;IACZ,aAAa;IACb,uBAAuB;IACvB,mBAAmB;IACnB,sBAAsB;IACtB,mBAAmB,EAAA;;AAIvB;EACE,SAAS,EAAA;;AAEX;EACE,YAAY,EAAA;;AAGd;EACE,UAAU;EACV,SAAS,EAAA;;AAGX;EACE,WAAW;EACX,SAAS,EAAA;;AAGX;EACE,UAAU;EACV,YAAY,EAAA;;AAGd;EACE,YAAY;EACZ,WAAW,EAAA;;AAGb;;EAEE,mBAAmB;EACnB,kBAAkB,EAAA;;AAGpB;EACE,WAAW;EACX,UAAU;EACV,SAAS,EAAA;;AAGX;EACE,WAAW;EACX,UAAU;EACV,YAAY;EACZ,oBAAoB,EAAA;;AC9EtB;EACE,yBCHe;EDIf,8BCHoB,EAAA;EDCtB;IAKI,yBCWmB,EAAA;EDhBvB;IAQI,sBCSuB,EAAA;EDjB3B;IAWI,yBCba,EAAA;;ADiBjB;EACE,yBCfe;EDgBf,8BCfoB,EAAA;EDatB;IAKI,yBCDmB,EAAA;EDJvB;IAQI,sBCHuB,EAAA;EDL3B;IAWI,yBCzBa,EAAA;;AD6BjB;EACE,yBC3Bc;ED4Bd,8BC3BmB,EAAA;EDyBrB;IAKI,yBCbkB,EAAA;EDQtB;IAQI,sBCfsB,EAAA;EDO1B;IAWI,yBCrCY,EAAA;;ADyChB;EACE,yBCvCY;EDwCZ,8BCvCiB,EAAA;EDqCnB;IAKI,yBCzBgB,EAAA;EDoBpB;IAQI,sBC3BoB,EAAA;EDmBxB;IAWI,yBCjDU,EAAA;;ADqDd;EACE,yBCnDe;EDoDf,8BCnDoB,EAAA;EDiDtB;IAKI,yBCrCmB,EAAA;EDgCvB;IAQI,sBCvCuB,EAAA;ED+B3B;IAWI,yBC7Da,EAAA;;ADiEjB;EACE,yBC/De;EDgEf,8BC/DoB,EAAA;ED6DtB;IAKI,yBCjDmB,EAAA;ED4CvB;IAQI,sBCnDuB,EAAA;ED2C3B;IAWI,yBCzEa,EAAA;;ACZjB;EACE;IAAK,WAAW,EAAA;EAChB;IAAO,SAAS,EAAA,EAAA;;AAGlB;EACE,eAAe;EACf,aAAa;EACb,oBAAoB;EACpB,WAAW;EACX,YAAY,EAAA;;AAGd;EACE,aAAa;EACb,kBAAkB;EAClB,kBAAkB;EAClB,mBAAmB;EACnB,0CAA0C;EAC1C,eAAe,EAAA;EANjB;IASI,WAAW;IACX,WAAW;IACX,gBAAgB;IAChB,kBAAkB,EAAA;IAZtB;MAeM,WAAW;MACX,kBAAkB,EAAA;EAhBxB;IAqBI,WAAW;IACX,gBAAgB;IAChB,eAAe;IACf,eAAe;IACf,kBAAkB,EAAA;EAzBtB;IA6BI,WAAW;IACX,4BAA4B;IAC5B,eAAe;IACf,iBAAiB;IACjB,qBAAqB;IACrB,gBAAgB;IAChB,aAAa,EAAA;EAnCjB;IAuCI,iBAAiB;IACjB,qBAAqB;IACrB,WAAW,EAAA;EAzCf;IA6CI,WAAW;IACX,YAAY;IACZ,kBAAkB;IAClB,qBAAqB;IACrB,kBAAkB;IAClB,WAAW;IACX,SAAS,EAAA;IAnDb;MAsDM,cAAc;MACd,kBAAkB;MAClB,gCAAgC;MAChC,WAAW;MACX,eAAe;MACf,SAAS;MACT,QAAQ,EAAA;;AAKd;;;;EAIE,eAAe;EACf,WAAW,EAAA;;AAGb;;EAEE,iBAAiB,EAAA;;AAGnB;;EAEE,kBAAkB,EAAA;;AAGpB;;EAEE,iBAAiB;EACjB,kBAAkB,EAAA","file":"notification.scss","sourcesContent":[".notification-container-top-center,\r\n.notification-container-top-left,\r\n.notification-container-top-right,\r\n.notification-container-bottom-center,\r\n.notification-container-bottom-left,\r\n.notification-container-bottom-right,\r\n.notification-container-center {\r\n  width: 325px;\r\n  position: absolute;\r\n  pointer-events: all;\r\n}\r\n\r\n.notification-container-center,\r\n.notification-container-top-center,\r\n.notification-container-bottom-center {\r\n  display: flex;\r\n  justify-content: center;\r\n  align-items: center;\r\n  flex-direction: column;\r\n  left: calc(50% - 162.5px);\r\n}\r\n\r\n.notification-container-center {\r\n  top: 20px;\r\n  height: 100%;\r\n  pointer-events: none;\r\n\r\n  .center-inner {\r\n    width: 325px;\r\n    display: flex;\r\n    justify-content: center;\r\n    align-items: center;\r\n    flex-direction: column;\r\n    pointer-events: all;\r\n  }\r\n}\r\n\r\n.notification-container-top-center {\r\n  top: 20px;\r\n}\r\n.notification-container-bottom-center {\r\n  bottom: 20px;\r\n}\r\n\r\n.notification-container-top-left {\r\n  left: 20px;\r\n  top: 20px;\r\n}\r\n\r\n.notification-container-top-right {\r\n  right: 20px;\r\n  top: 20px;\r\n}\r\n\r\n.notification-container-bottom-left {\r\n  left: 20px;\r\n  bottom: 20px;\r\n}\r\n\r\n.notification-container-bottom-right {\r\n  bottom: 20px;\r\n  right: 20px;\r\n}\r\n\r\n.notification-container-mobile-top,\r\n.notification-container-mobile-bottom {\r\n  pointer-events: all;\r\n  position: absolute;\r\n}\r\n\r\n.notification-container-mobile-top {\r\n  right: 20px;\r\n  left: 20px;\r\n  top: 20px;\r\n}\r\n\r\n.notification-container-mobile-bottom {\r\n  right: 20px;\r\n  left: 20px;\r\n  bottom: 20px;\r\n  margin-bottom: -15px;\r\n}\r\n","@import \"_variables.scss\";\r\n\r\n.notification-default {\r\n  background-color: $default;\r\n  border-left: 8px solid $default_dark;\r\n\r\n  .timer {\r\n    background-color: $default_timer;\r\n  }\r\n  .timer-filler {\r\n    background-color: $default_timer_filler;\r\n  }\r\n  .notification-close {\r\n    background-color: $default;\r\n  }\r\n}\r\n\r\n.notification-success {\r\n  background-color: $success;\r\n  border-left: 8px solid $success_dark;\r\n\r\n  .timer {\r\n    background-color: $success_timer;\r\n  }\r\n  .timer-filler {\r\n    background-color: $success_timer_filler;\r\n  }\r\n  .notification-close {\r\n    background-color: $success;\r\n  }\r\n}\r\n\r\n.notification-danger {\r\n  background-color: $danger;\r\n  border-left: 8px solid $danger_dark;\r\n\r\n  .timer {\r\n    background-color: $danger_timer;\r\n  }\r\n  .timer-filler {\r\n    background-color: $danger_timer_filler;\r\n  }\r\n  .notification-close {\r\n    background-color: $danger;\r\n  }\r\n}\r\n\r\n.notification-info {\r\n  background-color: $info;\r\n  border-left: 8px solid $info_dark;\r\n\r\n  .timer {\r\n    background-color: $info_timer;\r\n  }\r\n  .timer-filler {\r\n    background-color: $info_timer_filler;\r\n  }\r\n  .notification-close {\r\n    background-color: $info;\r\n  }\r\n}\r\n\r\n.notification-warning {\r\n  background-color: $warning;\r\n  border-left: 8px solid $warning_dark;\r\n\r\n  .timer {\r\n    background-color: $warning_timer;\r\n  }\r\n  .timer-filler {\r\n    background-color: $warning_timer_filler;\r\n  }\r\n  .notification-close {\r\n    background-color: $warning;\r\n  }\r\n}\r\n\r\n.notification-awesome {\r\n  background-color: $awesome;\r\n  border-left: 8px solid $awesome_dark;\r\n\r\n  .timer {\r\n    background-color: $awesome_timer;\r\n  }\r\n  .timer-filler {\r\n    background-color: $awesome_timer_filler;\r\n  }\r\n  .notification-close {\r\n    background-color: $awesome;\r\n  }\r\n}","$default: #007bff !default;\r\n$default_dark: #0562c7 !default;\r\n\r\n$success: #28a745 !default;\r\n$success_dark: #1f8838 !default;\r\n\r\n$danger: #dc3545 !default;\r\n$danger_dark: #bd1120 !default;\r\n\r\n$info: #17a2b8 !default;\r\n$info_dark: #138b9e !default;\r\n\r\n$warning: #eab000 !default;\r\n$warning_dark: #ce9c09 !default;\r\n\r\n$awesome: #685dc3 !default;\r\n$awesome_dark: #4c3fb1 !default;\r\n\r\n$default_timer: #007bff !default;\r\n$default_timer_filler: #fff !default;\r\n\r\n$success_timer: #28a745 !default;\r\n$success_timer_filler: #fff !default;\r\n\r\n$danger_timer: #dc3545 !default;\r\n$danger_timer_filler: #fff !default;\r\n\r\n$info_timer: #17a2b8 !default;\r\n$info_timer_filler: #fff !default;\r\n\r\n$warning_timer: #eab000 !default;\r\n$warning_timer_filler: #fff !default;\r\n\r\n$awesome_timer: #685dc3 !default;\r\n$awesome_timer_filler: #fff !default;\r\n","@import \"./_containers.scss\";\r\n@import \"./_types.scss\";\r\n\r\n@keyframes timer {\r\n  0% { width: 100%; }\r\n  100% { width: 0%; }\r\n}\r\n\r\n.react-notification-root {\r\n  position: fixed;\r\n  z-index: 9000;\r\n  pointer-events: none;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n\r\n.notification-item {\r\n  display: flex;\r\n  position: relative;\r\n  border-radius: 3px;\r\n  margin-bottom: 15px;\r\n  box-shadow: 1px 3px 4px rgba(0, 0, 0, 0.2);\r\n  cursor: pointer;\r\n\r\n  .timer {\r\n    width: 100%;\r\n    height: 3px;\r\n    margin-top: 10px;\r\n    border-radius: 5px;\r\n\r\n    .timer-filler {\r\n      height: 3px;\r\n      border-radius: 5px;\r\n    }\r\n  }\r\n\r\n  .notification-title {\r\n    color: #fff;\r\n    font-weight: 700;\r\n    font-size: 14px;\r\n    margin-top: 5px;\r\n    margin-bottom: 5px;\r\n  }\r\n\r\n  .notification-message {\r\n    color: #fff;\r\n    max-width: calc(100% - 15px);\r\n    font-size: 14px;\r\n    line-height: 150%;\r\n    word-wrap: break-word;\r\n    margin-bottom: 0;\r\n    margin-top: 0;\r\n  }\r\n\r\n  .notification-content {\r\n    padding: 8px 15px;\r\n    display: inline-block;\r\n    width: 100%;\r\n  }\r\n\r\n  .notification-close {\r\n    width: 18px;\r\n    height: 18px;\r\n    border-radius: 50%;\r\n    display: inline-block;\r\n    position: absolute;\r\n    right: 10px;\r\n    top: 10px;\r\n\r\n    &::after {\r\n      content: '\\D7';\r\n      position: absolute;\r\n      transform: translate(-50%, -50%);\r\n      color: #fff;\r\n      font-size: 12px;\r\n      left: 50%;\r\n      top: 50%;\r\n    }\r\n  }\r\n}\r\n\r\n.notification-container-mobile-top .notification-item,\r\n.notification-container-mobile-bottom .notification-item,\r\n.notification-container-mobile-top .notification-parent,\r\n.notification-container-mobile-bottom .notification-parent {\r\n  max-width: 100%;\r\n  width: 100%;\r\n}\r\n\r\n.notification-container-top-right .notification-parent,\r\n.notification-container-bottom-right .notification-parent {\r\n  margin-left: auto;\r\n}\r\n\r\n.notification-container-top-left .notification-parent,\r\n.notification-container-bottom-left .notification-parent {\r\n  margin-right: auto;\r\n}\r\n\r\n.notification-container-mobile-top .notification-parent,\r\n.notification-container-mobile-bottom .notification-parent {\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n}"]}]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./src/store/index.js":
/*!****************************!*\
  !*** ./src/store/index.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils_helpers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils/helpers */ "./src/utils/helpers.js");
/* harmony import */ var _utils_validators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/validators */ "./src/utils/validators.js");



function Store() {
  var _this = this;

  this.types = null;
  this.counter = 0;

  this.add = function () {};

  this.addNotification = function (notification) {
    var types = _this.types;

    if (true) {
      var transitions = ['slidingEnter', 'slidingExit', 'touchRevert', 'touchSlidingExit'];
      transitions.forEach(function (transition) {
        return Object(_utils_validators__WEBPACK_IMPORTED_MODULE_1__["validateTransition"])(notification, transition);
      });
      _utils_validators__WEBPACK_IMPORTED_MODULE_1__["validators"].forEach(function (validator) {
        return validator(notification, types);
      });
    }

    _this.counter += 1;
    return _this.add(Object(_utils_helpers__WEBPACK_IMPORTED_MODULE_0__["parseNotification"])(notification, types));
  };

  this.removeNotification = function () {};

  this.register = function (_ref) {
    var addNotification = _ref.addNotification,
        removeNotification = _ref.removeNotification,
        types = _ref.types;
    _this.add = addNotification;
    _this.removeNotification = removeNotification;
    _this.types = types;
  };

  return this;
}

/* harmony default export */ __webpack_exports__["default"] = (new Store());

/***/ }),

/***/ "./src/utils/constants.js":
/*!********************************!*\
  !*** ./src/utils/constants.js ***!
  \********************************/
/*! exports provided: NOTIFICATION_BASE_CLASS, CONTAINER, INSERTION, NOTIFICATION_TYPE, REMOVAL, ERROR */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NOTIFICATION_BASE_CLASS", function() { return NOTIFICATION_BASE_CLASS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CONTAINER", function() { return CONTAINER; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "INSERTION", function() { return INSERTION; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NOTIFICATION_TYPE", function() { return NOTIFICATION_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "REMOVAL", function() { return REMOVAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ERROR", function() { return ERROR; });
var NOTIFICATION_BASE_CLASS = 'notification-item';
var CONTAINER = {
  BOTTOM_LEFT: 'bottom-left',
  BOTTOM_RIGHT: 'bottom-right',
  BOTTOM_CENTER: 'bottom-center',
  TOP_LEFT: 'top-left',
  TOP_RIGHT: 'top-right',
  TOP_CENTER: 'top-center',
  CENTER: 'center'
};
var INSERTION = {
  TOP: 'top',
  BOTTOM: 'bottom'
};
var NOTIFICATION_TYPE = {
  SUCCESS: 'success',
  DANGER: 'danger',
  INFO: 'info',
  DEFAULT: 'default',
  WARNING: 'warning'
};
var REMOVAL = {
  TIMEOUT: 'timeout',
  CLICK: 'click',
  TOUCH: 'touch',
  MANUAL: 'manual'
};
var ERROR = {
  ANIMATION_IN: 'Validation error. `animationIn` option must be an array',
  ANIMATION_OUT: 'Validation error. `animationOut` option must be an array',
  DISMISS_REQUIRED: 'Validation error. `duration` property of `dismiss` option is required',
  DISMISS_NUMBER: 'Validation error. `duration` property of `dismiss` option must be a Number',
  DISMISS_POSITIVE: 'Validation error. `duration` property of `dismiss` option must be a positive Number',
  DISMISS_CLICK_BOOL: 'Validation error. `click` property of `dismiss` option must be a Boolean',
  DISMISS_TOUCH_BOOL: 'Validation error. `touch` property of `dismiss` option must be a Boolean',
  DISMISS_WAIT: 'Validation error. `waitForAnimation` property of `dismiss` option must be a Boolean',
  DISMISS_PAUSE_BOOL: 'Validation error. `pauseOnHover` property of `dismiss` option must be a Boolean',
  DISMISS_ONSCREEN_BOOL: 'Validation error. `onScreen` property of `dismiss` option must be a Boolean',
  DISMISS_ICON: 'Validation error. `showIcon` property of `dismiss` option must be a Boolean',
  TITLE_STRING: 'Validation error. `title` option must be a String',
  TITLE_ELEMENT: 'Validation error. `title` option must be a valid React element/function',
  MESSAGE_REQUIRED: 'Validation error. `message` option is required',
  MESSAGE_STRING: 'Validation error. `message` option must be a String',
  MESSAGE_ELEMENT: 'Validation error. `message` option must be a valid React element/function',
  TYPE_REQUIRED: 'Validation error. `type` option is required',
  TYPE_STRING: 'Validation error. `type` option must be a String',
  TYPE_NOT_EXISTENT: 'Validation error. `type` option not found',
  CONTAINER_REQUIRED: 'Validation error. `container` option is required',
  CONTAINER_STRING: 'Validation error. `container` option must be a String',
  CONTENT_INVALID: 'Validation error. `content` option must be a valid React component/function/element',
  WIDTH_NUMBER: 'Validation error. `width` option must be a Number',
  INSERT_STRING: 'Validation error. `insert` option must be a String',
  TRANSITION_DURATION_NUMBER: 'Validation error. `duration` property of `transition` option must be a Number',
  TRANSITION_TIMING_FUNCTION: 'Validation error. `timingFunction` property of `transition` option must be a String',
  TRANSITION_DELAY_NUMBER: 'Validation error. `delay` property of `transition` option must be a Number',
  TYPE_NOT_FOUND: 'Validation error. Custom type not found',
  REMOVAL_FUNC: 'Validation error. `onRemoval` must be a function'
};

/***/ }),

/***/ "./src/utils/helpers.js":
/*!******************************!*\
  !*** ./src/utils/helpers.js ***!
  \******************************/
/*! exports provided: isBottomContainer, isTopContainer, hasFullySwiped, shouldNotificationHaveSliding, htmlClassesForExistingType, getHtmlClassesForType, getNotificationsForMobileView, getNotificationsForEachContainer, getTransition, parseNotification */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isBottomContainer", function() { return isBottomContainer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isTopContainer", function() { return isTopContainer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hasFullySwiped", function() { return hasFullySwiped; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "shouldNotificationHaveSliding", function() { return shouldNotificationHaveSliding; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "htmlClassesForExistingType", function() { return htmlClassesForExistingType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getHtmlClassesForType", function() { return getHtmlClassesForType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getNotificationsForMobileView", function() { return getNotificationsForMobileView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getNotificationsForEachContainer", function() { return getNotificationsForEachContainer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTransition", function() { return getTransition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parseNotification", function() { return parseNotification; });
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../store */ "./src/store/index.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./constants */ "./src/utils/constants.js");



var isNull = function isNull(object) {
  return object === null || object === undefined;
};

function isBottomContainer(container) {
  return container === _constants__WEBPACK_IMPORTED_MODULE_1__["CONTAINER"].BOTTOM_LEFT || container === _constants__WEBPACK_IMPORTED_MODULE_1__["CONTAINER"].BOTTOM_RIGHT || container === _constants__WEBPACK_IMPORTED_MODULE_1__["CONTAINER"].BOTTOM_CENTER;
}
function isTopContainer(container) {
  return container === _constants__WEBPACK_IMPORTED_MODULE_1__["CONTAINER"].TOP_LEFT || container === _constants__WEBPACK_IMPORTED_MODULE_1__["CONTAINER"].TOP_RIGHT || container === _constants__WEBPACK_IMPORTED_MODULE_1__["CONTAINER"].TOP_CENTER;
}
function hasFullySwiped(diffX, width) {
  var swipeLength = Math.abs(diffX);
  var requiredSwipeLength = 40 / 100 * width;
  return swipeLength >= requiredSwipeLength;
}
function shouldNotificationHaveSliding(notification, count) {
  if (count <= 1) return false;
  return count > 1 && (notification.insert === _constants__WEBPACK_IMPORTED_MODULE_1__["INSERTION"].TOP && isTopContainer(notification.container) || notification.insert === _constants__WEBPACK_IMPORTED_MODULE_1__["INSERTION"].BOTTOM && isBottomContainer(notification.container) || notification.container === _constants__WEBPACK_IMPORTED_MODULE_1__["CONTAINER"].CENTER);
}
function htmlClassesForExistingType(type) {
  switch (type) {
    case _constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_TYPE"].DEFAULT:
      return [_constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_BASE_CLASS"], 'notification-default'];

    case _constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_TYPE"].SUCCESS:
      return [_constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_BASE_CLASS"], 'notification-success'];

    case _constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_TYPE"].DANGER:
      return [_constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_BASE_CLASS"], 'notification-danger'];

    case _constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_TYPE"].WARNING:
      return [_constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_BASE_CLASS"], 'notification-warning'];

    case _constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_TYPE"].INFO:
      return [_constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_BASE_CLASS"], 'notification-info'];

    default:
      return [_constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_BASE_CLASS"]];
  }
}
function getHtmlClassesForType(_ref) {
  var type = _ref.type,
      content = _ref.content,
      userDefinedTypes = _ref.userDefinedTypes;
  var base = [_constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_BASE_CLASS"]];
  if (content) return base;

  if (isNull(userDefinedTypes)) {
    return htmlClassesForExistingType(type);
  }

  var foundType = userDefinedTypes.find(function (q) {
    return q.name === type;
  });
  return base.concat(foundType.htmlClasses);
}
function getNotificationsForMobileView(notifications) {
  var top = [];
  var bottom = [];
  notifications.forEach(function (notification) {
    var container = notification.container;
    var CENTER = _constants__WEBPACK_IMPORTED_MODULE_1__["CONTAINER"].CENTER;

    if (isTopContainer(container) || container === CENTER) {
      top.push(notification);
    } else if (isBottomContainer(container)) {
      bottom.push(notification);
    }
  });
  return {
    top: top,
    bottom: bottom
  };
}
function getNotificationsForEachContainer(notifications) {
  var topLeft = [];
  var topRight = [];
  var topCenter = [];
  var bottomLeft = [];
  var bottomRight = [];
  var bottomCenter = [];
  var center = [];
  notifications.forEach(function (notification) {
    var container = notification.container;

    if (container === _constants__WEBPACK_IMPORTED_MODULE_1__["CONTAINER"].TOP_LEFT) {
      topLeft.push(notification);
    } else if (container === _constants__WEBPACK_IMPORTED_MODULE_1__["CONTAINER"].TOP_RIGHT) {
      topRight.push(notification);
    } else if (container === _constants__WEBPACK_IMPORTED_MODULE_1__["CONTAINER"].TOP_CENTER) {
      topCenter.push(notification);
    } else if (container === _constants__WEBPACK_IMPORTED_MODULE_1__["CONTAINER"].BOTTOM_LEFT) {
      bottomLeft.push(notification);
    } else if (container === _constants__WEBPACK_IMPORTED_MODULE_1__["CONTAINER"].BOTTOM_RIGHT) {
      bottomRight.push(notification);
    } else if (container === _constants__WEBPACK_IMPORTED_MODULE_1__["CONTAINER"].BOTTOM_CENTER) {
      bottomCenter.push(notification);
    } else if (container === _constants__WEBPACK_IMPORTED_MODULE_1__["CONTAINER"].CENTER) {
      center.push(notification);
    }
  });
  return {
    topLeft: topLeft,
    topRight: topRight,
    topCenter: topCenter,
    bottomLeft: bottomLeft,
    bottomRight: bottomRight,
    bottomCenter: bottomCenter,
    center: center
  };
}
function getTransition(_ref2, property) {
  var duration = _ref2.duration,
      timingFunction = _ref2.timingFunction,
      delay = _ref2.delay;
  return "".concat(duration, "ms ").concat(property, " ").concat(timingFunction, " ").concat(delay, "ms");
}

function defaultTransition(transition, _ref3) {
  var duration = _ref3.duration,
      timingFunction = _ref3.timingFunction,
      delay = _ref3.delay;
  var transitionOptions = transition || {};

  if (isNull(transitionOptions.duration)) {
    transitionOptions.duration = duration;
  }

  if (isNull(transitionOptions.timingFunction)) {
    transitionOptions.timingFunction = timingFunction;
  }

  if (isNull(transitionOptions.delay)) {
    transitionOptions.delay = delay;
  }

  return transitionOptions;
}

function defaultDismiss(dismiss) {
  var option = dismiss;
  var defaults = {
    duration: 0,
    click: true,
    touch: true,
    onScreen: false,
    waitForAnimation: false,
    showIcon: false
  };

  if (!option) {
    return defaults;
  }

  Object.keys(defaults).forEach(function (prop) {
    if (isNull(option[prop])) {
      option[prop] = defaults[prop];
    }
  });
  return option;
}

function defaultUserDefinedTypes(_ref4, definedTypes) {
  var content = _ref4.content,
      type = _ref4.type;
  if (content) return undefined;
  if (type === _constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_TYPE"].SUCCESS || type === _constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_TYPE"].DANGER || type === _constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_TYPE"].INFO || type === _constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_TYPE"].DEFAULT || type === _constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_TYPE"].WARNING || !definedTypes) return undefined;
  return definedTypes;
}

function parseNotification(options, userDefinedTypes) {
  var notification = options;
  var id = notification.id,
      type = notification.type,
      insert = notification.insert,
      content = notification.content,
      container = notification.container,
      animationIn = notification.animationIn,
      animationOut = notification.animationOut,
      slidingEnter = notification.slidingEnter,
      slidingExit = notification.slidingExit,
      touchRevert = notification.touchRevert,
      touchSlidingExit = notification.touchSlidingExit,
      dismiss = notification.dismiss,
      width = notification.width,
      onRemoval = notification.onRemoval;
  notification.id = id || _store__WEBPACK_IMPORTED_MODULE_0__["default"].counter;
  notification.type = content ? null : type.toLowerCase();

  if (userDefinedTypes && !content) {
    notification.userDefinedTypes = defaultUserDefinedTypes(notification, userDefinedTypes);
  }

  if (!isNull(width)) {
    notification.width = width;
  }

  notification.container = container.toLowerCase();
  notification.insert = (insert || 'top').toLowerCase();
  notification.dismiss = defaultDismiss(dismiss);
  notification.animationIn = animationIn || [];
  notification.animationOut = animationOut || [];

  notification.onRemoval = onRemoval || function () {};

  var t = function t(duration, timingFunction, delay) {
    return {
      duration: duration,
      timingFunction: timingFunction,
      delay: delay
    };
  };

  notification.slidingEnter = defaultTransition(slidingEnter, t(600, 'linear', 0));
  notification.slidingExit = defaultTransition(slidingExit, t(600, 'linear', 0));
  notification.touchRevert = defaultTransition(touchRevert, t(600, 'linear', 0));
  var touchExit = touchSlidingExit || {};
  var swipe = touchExit.swipe,
      fade = touchExit.fade;
  notification.touchSlidingExit = touchExit;
  notification.touchSlidingExit.swipe = defaultTransition(swipe || {}, t(600, 'linear', 0));
  notification.touchSlidingExit.fade = defaultTransition(fade || {}, t(300, 'linear', 0));
  return notification;
}

/***/ }),

/***/ "./src/utils/timer.js":
/*!****************************!*\
  !*** ./src/utils/timer.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (function (callback, delay) {
  var timerId;
  var start;
  var remaining = delay;

  this.pause = function () {
    clearTimeout(timerId);
    remaining -= Date.now() - start;
  };

  this.resume = function () {
    start = Date.now();
    clearTimeout(timerId);
    timerId = setTimeout(callback, remaining);
  };

  this.clear = function () {
    clearTimeout(timerId);
  };

  this.resume();
});

/***/ }),

/***/ "./src/utils/validators.js":
/*!*********************************!*\
  !*** ./src/utils/validators.js ***!
  \*********************************/
/*! exports provided: validateTransition, validators */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validateTransition", function() { return validateTransition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validators", function() { return validators; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./constants */ "./src/utils/constants.js");



var isNull = function isNull(object) {
  return object === null || object === undefined;
};

var isString = function isString(object) {
  return typeof object === 'string';
};

var isNumber = function isNumber(object) {
  return typeof object === 'number';
};

var isBoolean = function isBoolean(object) {
  return typeof object === 'boolean';
};

var isFunction = function isFunction(object) {
  return !!(object && object.constructor && object.call && object.apply);
};

var isArray = function isArray(object) {
  return !isNull(object) && object.constructor === Array;
};

function isClassComponent(component) {
  return typeof component === 'function' && !!component.prototype.isReactComponent;
}

function isFunctionComponent(component) {
  return typeof component === 'function';
}

var isReactElement = function isReactElement(value) {
  return isFunctionComponent(value) || /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.isValidElement(value);
};

function validateTransition(notification, transition) {
  var TRANSITION_DURATION_NUMBER = _constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].TRANSITION_DURATION_NUMBER,
      TRANSITION_TIMING_FUNCTION = _constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].TRANSITION_TIMING_FUNCTION,
      TRANSITION_DELAY_NUMBER = _constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].TRANSITION_DELAY_NUMBER;

  var _ref = notification[transition] || {},
      duration = _ref.duration,
      timingFunction = _ref.timingFunction,
      delay = _ref.delay;

  if (!isNull(duration) && !isNumber(duration)) {
    throw new Error(TRANSITION_DURATION_NUMBER.replace('transition', transition));
  }

  if (!isNull(timingFunction) && !isString(timingFunction)) {
    throw new Error(TRANSITION_TIMING_FUNCTION.replace('transition', transition));
  }

  if (!isNull(delay) && !isNumber(delay)) {
    throw new Error(TRANSITION_DELAY_NUMBER.replace('transition', transition));
  }
}
var validators = [function title(_ref2) {
  var content = _ref2.content,
      title = _ref2.title;
  return function () {
    if (content) return;
    if (isNull(title)) return;
    var isReactEl = isReactElement(title);
    if (isReactEl || typeof title === 'string') return;
    if (!isReactEl) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].TITLE_ELEMENT);
    if (typeof title !== 'string') throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].TITLE_STRING);
  }();
}, function message(_ref3) {
  var content = _ref3.content,
      message = _ref3.message;
  return function () {
    if (content) return;

    if (!message) {
      throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].MESSAGE_REQUIRED);
    }

    var isReactEl = isReactElement(message);
    if (isString(message) || isReactEl) return;
    if (!isString(message)) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].MESSAGE_STRING);
    if (!isReactEl) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].MESSAGE_ELEMENT);
  }();
}, function type(_ref4, userDefinedTypes) {
  var content = _ref4.content,
      type = _ref4.type;
  return function () {
    if (content) return;
    if (!type) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].TYPE_REQUIRED);
    if (!isString(type)) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].TYPE_STRING);

    if (!userDefinedTypes && type !== _constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_TYPE"].SUCCESS && type !== _constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_TYPE"].DANGER && type !== _constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_TYPE"].INFO && type !== _constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_TYPE"].DEFAULT && type !== _constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_TYPE"].WARNING) {
      throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].TYPE_NOT_EXISTENT);
    }
  }();
}, function container(_ref5) {
  var container = _ref5.container;
  return function () {
    if (isNull(container)) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].CONTAINER_REQUIRED);
    if (!isString(container)) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].CONTAINER_STRING);
  }();
}, function insert(_ref6) {
  var insert = _ref6.insert;
  return function () {
    if (isNull(insert)) return;
    if (!isString(insert)) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].INSERT_STRING);
  }();
}, function width(_ref7) {
  var width = _ref7.width;
  return function () {
    if (isNull(width)) return;
    if (!isNumber(width)) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].WIDTH_NUMBER);
  }();
}, function userDefinedTypes(_ref8, userDefinedTypes) {
  var type = _ref8.type,
      content = _ref8.content;
  if (content) return;
  if (type === _constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_TYPE"].SUCCESS || type === _constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_TYPE"].DANGER || type === _constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_TYPE"].INFO || type === _constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_TYPE"].DEFAULT || type === _constants__WEBPACK_IMPORTED_MODULE_1__["NOTIFICATION_TYPE"].WARNING || !userDefinedTypes) return;

  if (!userDefinedTypes.find(function (p) {
    return p.name === type;
  })) {
    throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].TYPE_NOT_FOUND);
  }
}, function content(_ref9) {
  var content = _ref9.content;
  return function () {
    if (!content) return;
    var isClass = isClassComponent(content);
    var isFunction = isFunctionComponent(content);
    var isElem = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.isValidElement(content);
    if (!isClass && !isFunction && !isElem) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].CONTENT_INVALID);
  }();
}, function animationIn(_ref10) {
  var animationIn = _ref10.animationIn;
  return function () {
    if (isNull(animationIn)) return;
    if (!isArray(animationIn)) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].ANIMATION_IN);
  }();
}, function animationOut(_ref11) {
  var animationOut = _ref11.animationOut;
  return function () {
    if (isNull(animationOut)) return;
    if (!isArray(animationOut)) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].ANIMATION_OUT);
  }();
}, function onRemoval(_ref12) {
  var onRemoval = _ref12.onRemoval;
  return function () {
    if (!onRemoval) return;
    if (!isFunction(onRemoval)) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].REMOVAL_FUNC);
  }();
}, function dismiss(_ref13) {
  var dismiss = _ref13.dismiss;
  return function () {
    if (!dismiss) return;
    var _dismiss = dismiss,
        duration = _dismiss.duration,
        onScreen = _dismiss.onScreen,
        showIcon = _dismiss.showIcon,
        pauseOnHover = _dismiss.pauseOnHover,
        wait = _dismiss.waitForAnimation,
        click = _dismiss.click,
        touch = _dismiss.touch;
    if (isNull(duration)) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].DISMISS_REQUIRED);
    if (!isNumber(duration)) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].DISMISS_NUMBER);
    if (duration < 0) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].DISMISS_POSITIVE);
    if (!isNull(onScreen) && !isBoolean(onScreen)) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].DISMISS_ONSCREEN_BOOL);
    if (!isNull(pauseOnHover) && !isBoolean(pauseOnHover)) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].DISMISS_PAUSE_BOOL);
    if (!isNull(click) && !isBoolean(click)) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].DISMISS_CLICK_BOOL);
    if (!isNull(touch) && !isBoolean(touch)) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].DISMISS_TOUCH_BOOL);
    if (!isNull(showIcon) && !isBoolean(showIcon)) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].DISMISS_ICON);
    if (!isNull(wait) && !isBoolean(wait)) throw new Error(_constants__WEBPACK_IMPORTED_MODULE_1__["ERROR"].DISMISS_WAIT);
  }();
}];

/***/ }),

/***/ "react":
/*!**************************************************************************************!*\
  !*** external {"commonjs":"react","commonjs2":"react","amd":"react","root":"React"} ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ })

/******/ });
//# sourceMappingURL=react-notifications.dev.js.map